
@extends('layouts.app')
@section('content')
    <div class="card mb-3">
        <div class="row g-0">
            <div class="col-md-8">
                <div class="card-body">
                    <p class="card-text">{{$news->content}}</p>
                    <h5 class="card-title" style="margin: 5px">Author: {{$news->user->name}}</h5>
                    <p class="card-text"><small class="text-muted">Date of creation {{ $news->created_at->format('d M Y') }}</small></p>
                    <p class="card-text"><small class="text-muted">Date of publication {{ date($news->date_publication) }}</small></p>
                    <div class="btn-group d-flex align-items-center">
                        @can('update', $news)
                        <a href="{{route('news.edit', ['news' => $news])}}" class=" btn-outline-info btn-sm">Edit</a>
                        @endcan
                        @can('delete', $news)
                        <form action="{{route('news.destroy',['news' => $news])}}" method="post">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-outline-danger btn-sm" type="submit">Remove</button>
                        </form>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($comments->count() > 0)
        <div class="list-group">
            @foreach($comments as $comment)
                <div class="list-group-item list-group-item-action shadow-lg">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="w-75">
                            <h5 class="mb-1">
                                <a href="" class="text-decoration-none">{{ $comment->user->name }}</a>
                            </h5>
                            <p class="mb-1"><em>{{ $comment->body }}</em></p>

                            <div class="accordion accordion-flush" id="accordionFlushExample">
                                <div class="accordion-item">
                                    <div id="flush-collapse{{ $comment->id }}" class="accordion-collapse collapse"
                                         aria-labelledby="flush-heading"
                                         data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body">
                                            <form
                                                action="{{ route('news.comments.update', ['news' => $news, 'comment' => $comment]) }}"
                                                method="POST">
                                                @csrf
                                                @method('PUT')
                                                <div class="row">
                                                    <div class="input-group">
                                                    <textarea class="form-control border-info"
                                                              name="body"
                                                              aria-label="With textarea"
                                                              cols="30" rows="5">{{ $comment->body }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="row mt-2 mb-5">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-outline-info">Edit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <small class="text-muted">{{ $comment->updated_at->diffForHumans() }}</small>
                            @can('delete', $comment)
                                <form
                                    action="{{ route('news.comments.destroy', ['news' => $news, 'comment' => $comment]) }}"
                                    method="POST" class="text-end">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-outline-info btn-sm" type="button" data-toggle="collapse"
                                            data-target="#flush-collapse{{ $comment->id }}" aria-expanded="false"
                                            aria-controls="flush-collapse{{ $comment->id }}">
                                        Edit
                                    </button>
                                    <button type="submit" class="btn btn-outline-danger btn-sm">
                                        Remove
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row justify-content-md-center mt-5">
            <div class="col-md-auto">
                {{ $comments->withQueryString()->links('pagination::bootstrap-4') }}
            </div>
        </div>
    @endif
    @if(\Illuminate\Support\Facades\Auth::check())
    @if(Auth::user() != $news->user)
        <div>
            <h3 class="mt-5 mb-3 bg-white-heading">Add comment</h3>
            <form action="{{ route('news.comments.store', $news) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="input-group">
                        <textarea class="form-control border-info @error('body') is-invalid border-danger @enderror"
                                  name="body" aria-label="With textarea"
                                  cols="30" rows="5" ></textarea>
                    </div>
                    @error('body')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="row mt-2 mb-5">
                    <div class="col-12">
                        <button type="submit" class="btn-bg-white">Add</button>
                    </div>
                </div>
            </form>
        </div>
    @endif
    @endif
    <a href="{{route('news.index')}}" class="btn-bg-white">Back to all news</a>

@endsection
