@extends('layouts.app')
@section('content')
    <div>
        <h1>Edit news</h1>
        <form enctype="multipart/form-data" method="post" action="{{ route('news.update', ['news' => $news]) }}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="contentNews"><b class="text-shadow-white">News content</b></label>
                <textarea class="form-control" name="content" id="contentNews" cols="30" rows="10" >{{$news->content}}</textarea>
                @error('content')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="category"><b class="text-shadow-white">Select a category:</b></label>
                <select class="form-control border-success  @error('category_id') is-invalid border-danger @enderror"
                        id="category"  name="category_id">
                    <option value="" selected >'Choose'</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}" @if($category == $news->category) selected @endif>{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            @error('category_id')
            <p class="text-danger">{{ $message }}</p>
            @enderror
            <label for="date_publication" class="text-shadow-white">Enter a date and time for your party booking:</label>
            <input id="date_publication" type="datetime-local" name="date_publication" value="{{$news->date_publication}}">
            @error('date_publication')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection


