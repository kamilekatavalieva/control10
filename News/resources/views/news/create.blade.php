@extends('layouts.app')
@section('content')
    <div>
        <h1>Create news</h1>
        <form enctype="multipart/form-data" method="post" action="{{ route('news.store') }}">
            @csrf
            <div class="form-group">
                <label for="contentNews"><b>News content</b></label>
                <textarea class="form-control" name="content" id="contentNews" cols="30" rows="10" ></textarea>
                @error('content')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            @if(Auth::user()->is_admin )
            <label for="date_publication">Enter a date and time for your party booking:</label>
            <input id="date_publication" type="datetime-local" name="date_publication" value="2022-02-02">
            @endif
            @error('date_publication')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
            <div class="form-group">
                <label for="category"><b>Select a category:</b></label>
                <select class="form-control border-success  @error('category_id') is-invalid border-danger @enderror"
                        id="category"  name="category_id">
                    <option value="" selected >'Choose'</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            @error('category_id')
            <p class="text-danger">{{ $message }}</p>
            @enderror

            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@endsection


