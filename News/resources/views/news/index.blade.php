@extends('layouts.app')
@section('content')

    <h1>All news</h1>
    @if(\Illuminate\Support\Facades\Auth::check())
        <div>
            <a href="{{route('news.create')}}" type="button" class="btn btn-sm btn-secondary" >Add news</a>
        </div>
    @endif

    <div class="">
        <form id="form_id" action="{{ route('news.index') }}" method="GET">
            <select name="category_id" class="form-select" aria-label="Default select example" onchange="this.form.submit()">
                <option selected disabled>Choose category</option>
                @foreach($categories as $category)
                    <option @if(isset($_GET['category_id'])) @if($_GET['category_id'] == $category->id)
                            selected @endif @endif value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </form>
    </div>

    <div class="row">
        @if($news->count() > 0)
            @foreach($news as $one_news)
                    <div class="col-4">
                        <div class="card" style="margin: 10px">
                            <div class="card-body">
                                <p class="card-category"><span class="font-weight-bold">Category:</span> {{ $one_news->category->name }}</p>
                                <p class="card-text">{{$one_news->content}}</p>
                                <p class="card-author">Author: {{$one_news->user->name}}</p>
                                @if(!Empty($one_news->category_id))

                                @endif
                                <p class="card-text"><span class="font-weight-bold">Date of creation:</span> {{ $one_news->created_at->format('d M Y ') }}</p>
                                <p class="card-text"><span class="font-weight-bold">Date of publication:</span> {{ date($one_news->date_publication) }}</p>
                                <a href="{{route('news.show', ['news' => $one_news])}}" class="card-link">Show more...</a>
                            </div>
                        </div>
                    </div>
            @endforeach
            <div class="row justify-content-md-center p-5">
                <div class="col-md-auto">
                    {{$news->links('pagination::bootstrap-4') }}
                </div>
            </div>
        @else
            <p>No published news</p>
        @endif
    </div>

@endsection

