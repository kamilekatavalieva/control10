<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\News;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;

class NewsTest extends TestCase
{
    /**
     * @var Collection|Model
     */
    private $user, $admin, $one_news, $category;

    /**
     *
     */
    protected function setUp():void
    {
        parent::setUp();

        $this->user = User::factory()->state(
            [
                'name' => 'User Username',
                'email' => $this->faker->email(),
                'password' => 'password',
                'is_admin' => false
            ]
        )->create();

        $this->admin = User::factory()->state(
            [
                'name' => 'Admin Admin',
                'email' => $this->faker->email(),
                'password' => 'admin',
                'is_admin' => true,
            ]
        )->create();

        $this->category = Category::factory()->create();

        $this->one_news = News::factory()->create();
    }

    /**
     * Success create news
     * @group news
     * @return void
     */
    public function test_success_create_news()
    {
        $this->actingAs($this->admin);
        $response = $this->get(route('news.create'));
        $response->assertOk();
        $data = [
            'content' => $this->faker->realTextBetween(10),
            'user_id' => $this->admin->id,
            'category_id' => $this->category->id,
        ];
        $response = $this->post(route('news.store'), $data);
        $this->assertDatabaseHas('news', $data);
        $response->assertRedirect(route('news.index'));

    }

    /**
     * Success admin can see news index
     * @group news
     * @return void
     */
    public function test_success_admin_can_see_news_index()
    {
        $this->actingAs($this->admin);
        $response = $this->get(route('news.index'));
        $response->assertOk();
    }

    /**
     * Success admin can edit news
     * @group news
     * @return void
     */
    public function test_success_admin_can_edit_news()
    {
        $this->actingAs($this->admin);
        $response = $this->get(route('news.edit', $this->one_news));
        $response->assertOk();
        $data = [
            'content' => $this->faker->realTextBetween(10),
            'user_id' => $this->user->id,
            'category_id' => $this->category->id,
        ];
        $response = $this->put(route('news.update', $this->one_news), $data);
        $response->assertRedirect(route('news.index'));
        $this->assertDatabaseHas('news', $data);
    }


    /**
     * Failed user can't edit news
     * @group news
     * @return void
     */
    public function test_failed_user_can_not_edit_news()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('news.edit', $this->one_news));
        $response->assertOk();
        $data = [
            'content' => $this->faker->realTextBetween(10),
            'user_id' => $this->user->id,
            'category_id' => $this->category->id,
        ];
        $response = $this->put(route('news.update', $this->one_news), $data);
        $response->assertStatus(403);
    }

    /**
     * Success admin can delete news
     * @group news
     * @return void
     */
    public function test_success_admin_can_delete_news()
    {
        $this->actingAs($this->admin);
        $response = $this->delete(route('news.destroy', $this->one_news));
        $this->assertDeleted($this->one_news);
        $response->assertRedirect(route('news.index'));

    }


    /**
     * Failed user can't delete news
     * @group news
     * @return void
     */
    public function test_failed_user_can_not_delete_news()
    {
        $this->actingAs($this->user);
        $response = $this->delete(route('news.destroy', $this->one_news));
        $response->assertStatus(403);
    }

    /**
     * Failed if content is null news
     * @group news
     * @return void
     */
    public function test_failed_if_content_is_null()
    {
        $this->actingAs($this->admin);
        $data = [
            'content' => '',
            'user_id' => $this->user->id,
            'category_id' => $this->category->id,
        ];
        $response = $this->post(route('news.store'), $data);
        $response->assertSessionHasErrors(['content']);
    }

    /**
     * Failed if content less than 5
     * @group news
     * @return void
     */
    public function test_failed_if_content_less_than_5()
    {
        $this->actingAs($this->admin);
        $data = [
            'content' => 'qwe',
            'user_id' => $this->user->id,
            'category_id' => $this->category->id,
        ];
        $response = $this->post(route('news.store'), $data);
        $response->assertSessionHasErrors(['content']);
    }


}
