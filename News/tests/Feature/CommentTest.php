<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Comment;
use App\Models\News;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * @var Collection|Model
     */
    protected $user, $admin, $news, $comment, $category;

    /**
     *
     */
    protected function setUp():void
    {
        parent::setUp();

        $this->user = User::factory()->state(
            [
                'name' => 'User Username',
                'email' => $this->faker->email(),
                'password' => 'password',
                'is_admin' => false,
            ]
        )->create();

        $this->admin = User::factory()->state(
            [
                'name' => 'Admin Admin',
                'email' => $this->faker->email(),
                'password' => 'admin',
                'is_admin' => true,
            ]
        )->create();

        $this->category = Category::factory()->create();
        $this->news = News::factory()->create();

    }

    /**
     * Success create comment.
     * @group comments
     * @return void
     */
    public function test_success_create_comment()
    {
        $this->actingAs($this->user);
        $data = [
            'body' => $this->faker->realText,
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ];
        $this->post(route('news.comments.store', ['news' => $this->news]), $data)
            ->assertRedirect(route('news.show', $this->news))
            ->assertSessionDoesntHaveErrors();
        $this->assertDatabaseHas('comments', $data);
    }

    /**
     * Failed create comment if not auth.
     * @group comments
     * @return void
     */
    public function test_failed_create_comment_if_not_auth()
    {
        $data = [
            'body' => $this->faker->realText,
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ];
        $this->post(route('news.comments.store', ['news' => $this->news]), $data);
        $this->assertDatabaseMissing('comments', $data);
    }

    /**
     * Success admin update comment.
     * @group comments
     * @return void
     */
    public function test_success_admin_update_comment()
    {
        $this->actingAs($this->admin);
        $comment = Comment::factory()->state([
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ])->create();
        $data = [
            'body' => $this->faker->realText,
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ];
        $this->from(route('news.show', $this->news))->put(route('news.comments.update', ['news' => $this->news, 'comment' => $comment]), $data)
            ->assertRedirect(route('news.show', $this->news));
        $this->assertDatabaseHas('comments', $data);
    }

    /**
     * Success user update comment.
     * @group comments
     * @return void
     */
    public function test_success_user_update_comment()
    {
        $this->actingAs($this->user);
        $comment = Comment::factory()->state([
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ])->create();
        $data = [
            'body' => $this->faker->realText,
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ];
        $this->from(route('news.show', $this->news))->put(route('news.comments.update', ['news' => $this->news, 'comment' => $comment]), $data)
            ->assertRedirect(route('news.show', $this->news));
        $this->assertDatabaseHas('comments', $data);
    }

    /**
     * Success admin delete comment.
     * @group comments
     * @return void
     */
    public function test_success_admin_delete_comment()
    {
        $this->actingAs($this->admin);
        $comment = Comment::factory()->state([
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ])->create();

        $this->from(route('news.show', $this->news))->delete(route('news.comments.destroy', ['news' => $this->news, 'comment' => $comment]))
            ->assertRedirect(route('news.show', $this->news));
        $this->assertDeleted($comment);
        $this->assertDatabaseMissing('comments', $comment->attributesToArray());
    }

    /**
     * Success user delete comment.
     * @group comments
     * @return void
     */
    public function test_success_user_delete_comment()
    {
        $this->actingAs($this->user);
        $comment = Comment::factory()->state([
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ])->create();
        $this->from(route('news.show', $this->news))->delete(route('news.comments.destroy', ['news' => $this->news, 'comment' => $comment]))
            ->assertRedirect(route('news.show', $this->news));
        $this->assertDeleted($comment);
        $this->assertDatabaseMissing('comments', $comment->attributesToArray());
    }

    /**
     * Failed update comment if not auth.
     * @group comments
     * @return void
     */
    public function test_failed_update_comment_if_not_auth()
    {
        $comment = Comment::factory()->state([
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ])->create();
        $data = [
            'body' => $this->faker->realText,
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ];
        $this->put(route('news.comments.update', ['news' => $this->news, 'comment' => $comment]), $data);
        $this->assertDatabaseMissing('comments', $data);
    }

//    /**
//     * Failed delete comment if not auth.
//     * @group comments5
//     * @return void
//     */
//    public function test_failed_delete_comment_if_not_auth()
//    {
//        $this->delete(route('news.comments.destroy', ['news' => $this->news, 'comment' => $this->comment]));
//        $this->assertDatabaseHas('comments', $this->comment->attributesToArray());
//    }


    /**
     * Failed update of someone else's comment.
     * @group comments
     * @return void
     */
    public function test_failed_update_of_someone_elses_comment()
    {
        $this->actingAs($this->user);
        $another_user = $this->admin;
        $comment = Comment::factory()->state([
            'user_id' => $another_user->id,
            'news_id' => $this->news->id,
        ])->create();
        $data = [
            'body' =>  $this->faker->realText,
            'user_id' => $another_user->id,
            'news_id' => $this->news->id,
        ];

        $this->put(route('news.comments.update', ['news' => $this->news, 'comment' => $comment]), $data)
            ->assertForbidden();
        $this->assertDatabaseMissing('comments', $data);
    }

//    /**
//     * Failed delete of someone else's comment.
//     * @group comments
//     * @return void
//     */
//    public function test_failed_delete_of_someone_elses_comment()
//    {
//
//        $comment = Comment::factory()->state([
//            'body' =>  $this->faker->realText,
//            'user_id' => $this->admin->id,
//            'news_id' => $this->news->id,
//        ])->create();
//        $this->actingAs($this->user);
//        $this->delete(route('news.comments.destroy', ['news' => $this->news, 'comment' => $comment]))
//            ->assertForbidden();
//        $this->assertDatabaseHas('comments', $comment->attributesToArray());
//    }


    /**
     * Failed create comment if body field less than 2.
     * @group comments
     * @return void
     */
    public function test_failed_create_comment_if_body_field_less_than_two()
    {
        $this->actingAs($this->user);
        $data = [
            'body' => 'q',
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ];
        $this->post(route('news.comments.store', ['news' => $this->news]), $data)
            ->assertSessionHasErrors('body');
        $this->assertDatabaseMissing('comments', $data);
    }

    /**
     * Failed create comment if body field is empty.
     * @group comments
     * @return void
     */
    public function test_failed_create_comment_if_body_field_is_empty()
    {
        $this->actingAs($this->user);
        $data = [
            'body' => '',
            'user_id' => $this->user->id,
            'news_id' => $this->news->id,
        ];
        $this->post(route('news.comments.store', ['news' => $this->news]), $data)
            ->assertSessionHasErrors('body');
        $this->assertDatabaseMissing('comments', $data);
    }

}
