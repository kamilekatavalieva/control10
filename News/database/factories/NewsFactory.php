<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition():array
    {
        return [
            'content' => $this->faker->paragraph(4),
            'date_publication' => $this->faker->dateTimeBetween('-7 days', '+7 days') ,
            'user_id' => rand(1, 10),
            'category_id' => rand(1, 8),
        ];
    }
}
