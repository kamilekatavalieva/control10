<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\News;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $news = News::all();
        for ($i = 0; $i < 50; $i++) {
            Comment::factory()->for($users->random())->for($news->random())->create();
        }
    }
}
