<?php

namespace App\Http\Controllers;

use App\Filters\NewsFilter;
use App\Http\Requests\NewsRequest;
use App\Models\Category;
use App\Models\News;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(NewsFilter  $filter)
    {
        $now =  now();
        $news = News::filter($filter)->where('date_publication', '<=', $now)->paginate(6);
        $categories = Category::all();
        if (Auth::check()) {
            if(Auth::user()->is_admin) {
                $news = News::filter($filter)->orderBy('id', 'desc')->paginate(6);
                return view('news.index', compact('news', 'categories'));
            }
        }

        return view('news.index', compact('news','categories'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $categories = Category::all();
        return view('news.create', compact('categories'));
    }


    /**
     * @param NewsRequest $request
     * @return RedirectResponse
     */
    public function store(NewsRequest $request):RedirectResponse
    {
        $news = new News();
        $news->content = $request->input('content');
        $news->user()->associate($request->user());
        $news->date_publication = $request->input('date_publication');
        $news->category_id = $request->category_id;

        $news->save();
        return redirect()->route('news.index')->with('status', "News successfully created!");
    }


    /**
     * @param News $news
     * @return Application|Factory|View
     */
    public function show(News $news)
    {
        $comments = $news->comments()->orderByDesc('updated_at')->paginate(5);
        return view('news.show', compact('news', 'comments'));
    }


    /**
     * @param News $news
     * @return Application|Factory|View
     */
    public function edit(News $news)
    {
        $categories = Category::all();
        return view('news.edit', compact('news', 'categories'));
    }


    /**
     * @param NewsRequest $request
     * @param News $news
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(NewsRequest $request, News $news): RedirectResponse
    {
        $this->authorize('update', $news);
        $data = $request->all();
        $news->update($data);

        return redirect()->route('news.index')->with('status', 'News successfully updated!');
    }


    /**
     * @param News $news
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(News $news):RedirectResponse
    {
        $this->authorize('delete', $news);
        $news->delete();
        return redirect()->route('news.index')->with('status', 'Successfully deleted!');
    }
}
