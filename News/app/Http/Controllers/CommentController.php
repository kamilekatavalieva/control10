<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\News;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;

class CommentController extends Controller
{
    /**
     * @param CommentRequest $request
     * @param News $news
     * @return RedirectResponse
     */
    public function store(CommentRequest $request, News $news): RedirectResponse
    {
        $data = $request->validated();
        $data['user_id'] = auth()->user()->id;
        $data['news_id'] = $news->id;

        Comment::create($data);

        return redirect()->route('news.show', $news->id)->with('status', "Comment successfully created!");
    }


    /**
     * @param News $news
     * @param CommentRequest $request
     * @param Comment $comment
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(News $news, CommentRequest $request, Comment $comment): RedirectResponse
    {
        $this->authorize('update', $comment);

        $data = $request->validated();
        $comment->update($data);

        return redirect()->back()->with('status', 'Comment successfully updated!');
    }


    /**
     * @param News $news
     * @param Comment $comment
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(News $news, Comment $comment): RedirectResponse
    {
        $this->authorize('delete', $comment);

        $comment->delete();
        return redirect()->back()->with('status', 'Successfully deleted!');
    }
}
