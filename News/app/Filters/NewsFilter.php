<?php

namespace App\Filters;

class NewsFilter extends QueryFilter
{
    /**
     * @param null $id
     * @return mixed
     */
    public function category_id($id = null){
        return $this->builder->when($id, function($query) use($id){
            $query->where('category_id', $id);
        });
    }

}
